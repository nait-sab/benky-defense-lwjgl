package design;

import org.newdawn.slick.opengl.Texture;

public class Bouton
{
    private String nom;
    private Texture texture;
    private int xPosition, yPosition, longueur, hauteur;

    public Bouton(String nom, Texture texture, int xPosition, int yPosition, int longueur, int hauteur)
    {
        this.nom = nom;
        this.texture = texture;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.longueur = longueur;
        this.hauteur = hauteur;
    }

    public Bouton(String nom, Texture texture, int xPosition, int yPosition)
    {
        this.nom = nom;
        this.texture = texture;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        longueur = texture.getImageWidth();
        hauteur = texture.getImageHeight();
    }

    // Accesseurs
    public String getNom() { return nom; }
    public Texture getTexture() { return texture; }
    public int getxPosition() { return xPosition; }
    public int getyPosition() { return yPosition; }
    public int getLongueur() { return longueur; }
    public int getHauteur() { return hauteur; }

    // Modifieurs
    public void setNom(String nom) { this.nom = nom; }
    public void setTexture(Texture texture) { this.texture = texture; }
    public void setxPosition(int xPosition) { this.xPosition = xPosition; }
    public void setyPosition(int yPosition) { this.yPosition = yPosition; }
    public void setLongueur(int longueur) { this.longueur = longueur; }
    public void setHauteur(int hauteur) { this.hauteur = hauteur; }
}
