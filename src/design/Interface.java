package design;

import org.lwjgl.input.Mouse;
import org.newdawn.slick.TrueTypeFont;

import java.awt.*;
import java.util.ArrayList;

import static assistants.GestionnaireGraphique.*;

public class Interface
{
    private ArrayList<Bouton> listeBoutons;
    private ArrayList<Menu> listeMenus;
    private TrueTypeFont police;
    private Font awtPolice;

    public Interface()
    {
        listeBoutons = new ArrayList<Bouton>();
        listeMenus = new ArrayList<Menu>();
        awtPolice = new Font("Times New Roman", Font.BOLD, 24);
        police = new TrueTypeFont(awtPolice, false);
    }

    public void Dessiner()
    {
        for (Bouton bouton : listeBoutons)
            DessinerTexture(bouton.getTexture(), bouton.getxPosition(), bouton.getyPosition(),
                    bouton.getLongueur(), bouton.getHauteur());

        for (Menu menu : listeMenus)
            menu.Dessiner();
    }

    public void DessinerTexte(int xPosition, int yPosition, String texte)
    {
        police.drawString(xPosition, yPosition, texte);
    }

    public void AjouterBouton(String nom, String nomTexture, int xPosition, int yPosition)
    {
        listeBoutons.add(new Bouton(nom, ChargerRapide(nomTexture), xPosition, yPosition));
    }

    public boolean BoutonClic(String nomBouton)
    {
        Bouton bouton = getBouton(nomBouton);
        float sourisY = HAUTEUR - Mouse.getY() - 1;

        if (Mouse.getX() > bouton.getxPosition() && Mouse.getX() < bouton.getxPosition() + bouton.getLongueur() &&
                sourisY > bouton.getyPosition() && sourisY < bouton.getyPosition() + bouton.getHauteur())
        {
            return true;
        }
        return false;
    }

    private Bouton getBouton(String nomBouton)
    {
        for (Bouton bouton : listeBoutons)
        {
            if (bouton.getNom().equals(nomBouton)) return bouton;
        }

        return null;
    }

    public void CreerMenu(String nom, int xPosition, int yPosition, int longueur, int hauteur,
                          int optionLongueur, int optionHauteur)
    {
        listeMenus.add(new Menu(nom, xPosition, yPosition, longueur, hauteur, optionLongueur, optionHauteur));
    }

    public Menu getMenu(String nom)
    {
        for (Menu menu : listeMenus)
        {
            if (nom .equals(menu.getNom()))
                return menu;
        }
        return null;
    }

    public class Menu
    {
        private String nom;
        private ArrayList<Bouton> boutonsMenu;
        private int xPosition, yPosition, longueur, hauteur, nombreBoutons, optionLongueur, optionHauteur, remplissage;

        public Menu(String nom, int xPosition, int yPosition, int longueur, int hauteur,
                    int optionLongueur, int optionHauteur)
        {
            this.nom = nom;
            this.xPosition = xPosition;
            this.yPosition = yPosition;
            this.longueur = longueur;
            this.hauteur = hauteur;
            this.optionLongueur = optionLongueur;
            this.optionHauteur = optionHauteur;
            remplissage = (longueur - (optionLongueur * TAILLECASE)) / (optionLongueur + 1);
            nombreBoutons = 0;
            boutonsMenu = new ArrayList<Bouton>();
        }

        public void AjouterBouton(Bouton bouton)
        {
            setBouton(bouton);
        }

        public void AjouterRapide(String nom, String nomTexture)
        {
            Bouton bouton = new Bouton(nom, ChargerRapide(nomTexture), 0, 0);
            setBouton(bouton);
        }

        private void setBouton(Bouton bouton)
        {
            if (optionLongueur != 0)
                bouton.setyPosition(yPosition + (nombreBoutons / optionLongueur) * TAILLECASE);
            bouton.setxPosition(xPosition + (nombreBoutons % optionLongueur) * (remplissage + TAILLECASE) + remplissage);
            nombreBoutons++;
            boutonsMenu.add(bouton);
        }

        public boolean BoutonClic(String nomBouton)
        {
            Bouton bouton = getBouton(nomBouton);
            float sourisY = HAUTEUR - Mouse.getY() - 1;

            if (Mouse.getX() > bouton.getxPosition() && Mouse.getX() < bouton.getxPosition() + bouton.getLongueur() &&
                    sourisY > bouton.getyPosition() && sourisY < bouton.getyPosition() + bouton.getHauteur())
            {
                return true;
            }
            return false;
        }

        public void Dessiner()
        {
            for (Bouton bouton : boutonsMenu)
                DessinerTexture(bouton.getTexture(), bouton.getxPosition(), bouton.getyPosition(),
                        bouton.getLongueur(), bouton.getHauteur());
        }

        private Bouton getBouton(String nomBouton)
        {
            for (Bouton bouton : boutonsMenu)
            {
                if (bouton.getNom().equals(nomBouton)) return bouton;
            }

            return null;
        }

        public String getNom() { return nom; }
    }
}
