package data.environnement;

import static assistants.GestionnaireGraphique.*;

// Classe : Grille
// Créer la carte du jeu
// Rappel : Les cases mesurent 64 pixels !

public class Grille
{
    // Variables
    private int longueurBord, hauteurBord;

    // Composant
    private Case[][] carte;

    // Constructeur défaut
    // Créer une carte d'herbe
    public Grille()
    {
        this.longueurBord = 20;
        this.hauteurBord = 15;

        // La carte a les dimensions de sa longueur, hauteur / 64
        carte = new Case[20][15];

        for (int yCase = 0; yCase < carte.length; yCase++)
        {
            for (int xCase = 0; xCase < carte[yCase].length; xCase++)
            {
                carte[yCase][xCase] = new Case(yCase * TAILLECASE, xCase * TAILLECASE,
                        TAILLECASE, TAILLECASE, CaseType.Herbe);
            }
        }
    }

    // Constructeur par plan
    // Créer une carte selon le plan donné
    public Grille(int[][] nouvelleCarte)
    {
        longueurBord = nouvelleCarte[0].length;
        hauteurBord = nouvelleCarte.length;

        carte = new Case[longueurBord][hauteurBord];

        for (int yCase = 0; yCase < carte.length; yCase++)
        {
            for (int xCase = 0; xCase < carte[yCase].length; xCase++)
            {
                switch (nouvelleCarte[xCase][yCase])
                {
                    case 0: // CASE HERBE
                        carte[yCase][xCase] = new Case(yCase * TAILLECASE, xCase * TAILLECASE,
                                TAILLECASE, TAILLECASE, CaseType.Herbe);
                        break;

                    case 1: // CASE TERRE
                        carte[yCase][xCase] = new Case(yCase * TAILLECASE, xCase * TAILLECASE,
                                TAILLECASE, TAILLECASE, CaseType.Terre);
                        break;

                    case 2: // CASE EAU
                        carte[yCase][xCase] = new Case(yCase * TAILLECASE, xCase * TAILLECASE,
                                TAILLECASE, TAILLECASE, CaseType.Eau);
                        break;
                }
            }
        }
    }

    // Fonction : Dessiner
    // Dessine les textures de la carte
    public void Dessiner()
    {
        for (int yCase = 0; yCase < carte.length; yCase++)
        {
            for (int xCase = 0; xCase < carte[yCase].length; xCase++)
            {
                carte[yCase][xCase].Dessiner();
            }
        }
    }

    // Accesseurs
    public Case getCase(int xEmplacement, int yEmplacement)
    {
        if (xEmplacement < longueurBord && yEmplacement < hauteurBord &&
                xEmplacement > -1 && yEmplacement > -1)
            return carte[xEmplacement][yEmplacement];
        else
            return new Case(0, 0, 0, 0, CaseType.NULL);
    }
    public int getLongueurBord() { return longueurBord; }
    public int getHauteurBord() { return hauteurBord; }

    // Modifieurs
    public void setCase(int xPosition, int yPosition, CaseType type)
    {
        carte[xPosition][yPosition] = new Case(xPosition * TAILLECASE, yPosition * TAILLECASE,
                TAILLECASE, TAILLECASE, type);
    }
}
