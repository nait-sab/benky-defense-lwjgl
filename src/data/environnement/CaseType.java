package data.environnement;

// Enumération Terrain
// Définir ici les types de cases avec le nom de l'image et si la case peut être utilisé par une structure ou non

public enum CaseType
{
    Herbe("Herbe", true),
    Terre("Terre", false),
    Eau("Eau", false),
    NULL("Eau", false);

    private String nomTexture;
    private boolean utilisable;

    CaseType(String nomTexture, boolean utilisable)
    {
        this.nomTexture = nomTexture;
        this.utilisable = utilisable;
    }

    // Accesseurs
    public String getNomTexture() { return nomTexture; }
    public boolean getUtilisable() { return utilisable; }
}
