package data.environnement;

// Classe : Point De controle
// Contient la position et la case du point de controle ou se dirige les ennemis

public class PointControle
{
    private Case emplacement;
    private int xDirection;
    private int yDirection;

    public PointControle(Case emplacement, int xDirection, int yDirection)
    {
        this.emplacement = emplacement;
        this.xDirection = xDirection;
        this.yDirection = yDirection;
    }

    // Accesseurs
    public Case getCase() { return emplacement; }
    public int getxDirection() { return xDirection; }
    public int getyDirection() { return yDirection; }
}
