package data.environnement;

import org.newdawn.slick.opengl.Texture;

import static assistants.GestionnaireGraphique.*;

// Classe Case
// Définit les coordonnées, textures et type de case du jeu

public class Case
{
    // Variables
    private float xPosition, yPosition;
    private int longueur, hauteur;
    private boolean utilisable;

    // Composants
    private Texture texture;
    private CaseType type;

    // Constructeur
    public Case(float xPosition, float yPosition, int longueur, int hauteur, CaseType type)
    {
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.longueur = longueur;
        this.hauteur = hauteur;
        this.type = type;
        this.texture = ChargerRapide(type.getNomTexture());
        if (type.getUtilisable())
            utilisable = false;
        else
            utilisable = true;
    }

    // Fonctions
    public void Dessiner()
    {
        DessinerTexture(getTexture(), getxPosition(), getyPosition(), getLongueur(), getHauteur());
    }

    // Accesseurs
    public float getxPosition() { return xPosition; }
    public float getyPosition() { return yPosition; }
    public int getxEmplacement() { return (int) xPosition / TAILLECASE;}
    public int getyEmplacement() { return (int) yPosition / TAILLECASE;}
    public int getLongueur() { return longueur; }
    public int getHauteur() { return hauteur; }
    public Texture getTexture() { return texture; }
    public CaseType getType() { return type; }
    public boolean getUtilisable() { return utilisable; }

    // Modifieurs
    public void setxPosition(float xPosition) { this.xPosition = xPosition; }
    public void setyPosition(float yPosition) { this.yPosition = yPosition; }
    public void setLongueur(int longueur) { this.longueur = longueur; }
    public void setHauteur(int hauteur) { this.hauteur = hauteur; }
    public void setTexture(Texture texture) { this.texture = texture; }
    public void setType(CaseType type) { this.type = type; }
    public void setUtilisable(boolean utilisable) { this.utilisable = utilisable; }
}
