package data.manches;

import data.entites.Ennemis.Ennemi;

// Classe : GestionnaireManche
// Dirige et génére les manches

public class GestionnaireManche
{
    // Variables
    private float tempsDerniereVague, tempsEntreEnnemis;
    private int nombreManche, ennemisParManche;

    // Composants
    private Ennemi[] ennemisType;
    private Manche mancheActuelle;

    // Constructeur
    public GestionnaireManche(Ennemi[] ennemisType, int ennemisParManche, float tempsEntreEnnemis)
    {
        tempsDerniereVague = 0;
        nombreManche = 0;

        this.ennemisType = ennemisType;
        this.ennemisParManche = ennemisParManche;
        this.tempsEntreEnnemis = tempsEntreEnnemis;

        mancheActuelle = null;
        NouvelleManche();
    }

    // Mise à jour
    public void Maj()
    {
        if (!mancheActuelle.getFinApparition())
        {
            mancheActuelle.Maj();
        }
        else
        {
            NouvelleManche();

            // Console
            System.out.println("DEBUG::GestionnaireManche::Vague " + nombreManche + " terminée");
        }
    }

    // Fonction : Nouvelle Manche
    // Créer une nouvelle manche et actualise le nombre de manche actuelle
    private void NouvelleManche()
    {
        mancheActuelle = new Manche(ennemisType, ennemisParManche, tempsEntreEnnemis);
        nombreManche++;

        // Console
        System.out.println("DEBUG::GestionnaireManche::Début de la vague " + nombreManche);
    }

    // Acceseurs
    public Manche getMancheActuelle() { return mancheActuelle; }
    public int getNombreManche() { return nombreManche; }
}
