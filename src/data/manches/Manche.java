package data.manches;

import data.entites.Ennemis.Ennemi;

import java.util.Random;
import java.util.concurrent.CopyOnWriteArrayList;

import static assistants.Horloge.*;
import static assistants.GestionnaireGraphique.*;

// Classe : Manche
// Créer les ennemis selon des réglages

public class Manche
{
    // Variables
    private float tempsDerniereApparition, tempsApparition;
    private int ennemisParManche, ennemisApparus;
    private boolean finApparition;

    // Composants
    private Ennemi[] ennemisType;

    // Liste
    private CopyOnWriteArrayList<Ennemi> listeEnnemis;

    // Constructeur
    public Manche(Ennemi[] ennemisType, int ennemisParManche, float tempsApparition)
    {
        tempsDerniereApparition = 0;

        this.tempsApparition = tempsApparition;
        this.ennemisType = ennemisType;
        this.ennemisParManche = ennemisParManche;
        ennemisApparus = 0;

        finApparition = false;
        listeEnnemis = new CopyOnWriteArrayList<Ennemi>();

        Apparition();
    }

    // Mise à jour
    public void Maj()
    {
        boolean ennemisMort = true;

        if (ennemisApparus < ennemisParManche)
        {
            tempsDerniereApparition += Temps();
            if (tempsDerniereApparition > tempsApparition)
            {
                Apparition();
                tempsDerniereApparition = 0;
            }
        }

        for (Ennemi ennemi : listeEnnemis)
        {
            if (ennemi.getVivant())
            {
                ennemisMort = false;
                ennemi.Maj();
                ennemi.Dessiner();
            }
            else
            {
                listeEnnemis.remove(ennemi);
            }
        }

        if (ennemisMort) finApparition = true;
    }

    // Fonction : Apparition
    // Créer un nouveau ennemi
    private void Apparition()
    {
        int ennemiChoisi = 0;
        Random choix = new Random();
        ennemiChoisi = choix.nextInt(ennemisType.length);

        listeEnnemis.add(new Ennemi(ennemisType[ennemiChoisi].getTexture(), ennemisType[ennemiChoisi].getCaseCreation(),
                ennemisType[ennemiChoisi].getGrille(), TAILLECASE, TAILLECASE,
                ennemisType[ennemiChoisi].getVitesse(), ennemisType[ennemiChoisi].getSante()));
        ennemisApparus++;
    }

    // Accesseurs
    public boolean getFinApparition() { return finApparition; }
    public CopyOnWriteArrayList<Ennemi> getListeEnnemis() { return listeEnnemis; }
}
