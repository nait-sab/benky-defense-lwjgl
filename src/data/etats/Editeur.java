package data.etats;

import assistants.GestionnaireEtat;
import data.entites.tours.Hiverneuse;
import data.entites.tours.TourType;
import data.environnement.Grille;
import data.environnement.CaseType;
import design.Interface;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.opengl.Texture;

import static assistants.GestionnaireGraphique.*;
import static assistants.GestionnaireNiveau.*;

public class Editeur
{
    // Variables
    private int index;

    // Composants
    private CaseType[] types;
    private Grille grille;
    private Interface editeurInterface;
    private Interface.Menu casesMenu;
    private Texture menuFond;

    // Constructeur
    public Editeur()
    {
        grille = ChargerCarte("carteEditeur");
        this.index = 0;
        this.types = new CaseType[3];
        this.types[0] = CaseType.Herbe;
        this.types[1] = CaseType.Terre;
        this.types[2] = CaseType.Eau;
        menuFond = ChargerRapide("toursFond");
        ConfigurerInterface();
    }

    private void ConfigurerInterface()
    {
        editeurInterface = new Interface();

        editeurInterface.CreerMenu("casesMenu", 1280, 50, 192, 960,
                2, 0);

        casesMenu = editeurInterface.getMenu("casesMenu");
        casesMenu.AjouterRapide("Herbe", "Herbe");
        casesMenu.AjouterRapide("Terre", "Terre");
        casesMenu.AjouterRapide("Eau", "Eau");
    }

    // Mise à jour
    public void Maj()
    {
        Dessiner();

        // Events
        Souris();
        Clavier();
    }

    // Rendu
    private void Dessiner()
    {
        DessinerTexture(menuFond, 1280, 0, 192, 960);
        grille.Dessiner();
        editeurInterface.Dessiner();
    }

    // Fonctions
    private void Souris()
    {
        // Souris événement : 0 - clic gauche / 1 - clic droit
        if (Mouse.next())
        {
            boolean clicGauche = Mouse.isButtonDown(0);

            if (clicGauche)
            {
                if (casesMenu.BoutonClic("Herbe"))
                    index = 0;
                else if (casesMenu.BoutonClic("Terre"))
                    index = 1;
                else if (casesMenu.BoutonClic("Eau"))
                    index = 2;
                else
                    setCase();
            }

            clicGauche = false;
        }
    }

    private void Clavier()
    {
        // Clavier événements
        while (Keyboard.next())
        {
            // Touche -
            // Ralenti le temps du jeu
            if (Keyboard.getEventKey() == Keyboard.KEY_DOWN && Keyboard.getEventKeyState())
                ChangerIndex();

            // Touche +
            // Accélère le temps du jeu
            if (Keyboard.getEventKey() == Keyboard.KEY_UP && Keyboard.getEventKeyState())
                ChangerIndex();

            // Touche S
            // Sauvegarde la carte
            if (Keyboard.getEventKey() == Keyboard.KEY_S && Keyboard.getEventKeyState())
                SauvegarderCarte("carteEditeur", grille);

            // Touche Echap
            // Revenir au menu principal
            if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE && Keyboard.getEventKeyState())
                GestionnaireEtat.DefinirEtat(GestionnaireEtat.JeuEtat.MenuPrincipal);
        }
    }

    private void ChangerIndex()
    {
        index++;
        if (index > types.length - 1) index = 0;
    }

    private void setCase()
    {
        grille.setCase( (int) Math.floor(Mouse.getX() / TAILLECASE),
                (int) Math.floor((HAUTEUR - Mouse.getY() - 1) / TAILLECASE), types[index]);
    }
}
