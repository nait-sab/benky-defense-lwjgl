package data.etats;

import assistants.GestionnaireEtat;
import design.Interface;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.opengl.Texture;

import static assistants.GestionnaireGraphique.*;

public class MenuPrincipal
{
    private Texture fond;
    private Interface menuInterface;

    public MenuPrincipal()
    {
        fond = ChargerRapide("menuPrincipal");
        menuInterface = new Interface();
        menuInterface.AjouterBouton("Jouer", "boutonJouer", LONGUEUR / 2 - 128,
                (int) (HAUTEUR * 0.45f));
        menuInterface.AjouterBouton("Editeur", "boutonEditeur", LONGUEUR / 2 - 128,
                (int) (HAUTEUR * 0.55f));
        menuInterface.AjouterBouton("Quitter", "boutonQuitter", LONGUEUR / 2 - 128,
                (int) (HAUTEUR * 0.65f));
    }

    public void Maj()
    {
        DessinerTexture(fond, 0, 0, 2048, 1024);
        menuInterface.Dessiner();
        MajBoutons();
    }

    public void MajBoutons()
    {
        if (Mouse.isButtonDown(0))
        {
            // Jouer
            if (menuInterface.BoutonClic("Jouer"))
                GestionnaireEtat.DefinirEtat(GestionnaireEtat.JeuEtat.Jeu);

            // Editeur
            if (menuInterface.BoutonClic("Editeur"))
                GestionnaireEtat.DefinirEtat(GestionnaireEtat.JeuEtat.Editeur);

            // Quitter
            if (menuInterface.BoutonClic("Quitter"))
                System.exit(0);
        }
    }
}
