package data.etats;

import assistants.GestionnaireEtat;
import data.entites.Ennemis.Drone;
import data.entites.Ennemis.Ennemi;
import data.entites.Ennemis.Soucoupe;
import data.entites.Joueur;
import data.entites.tours.Hiverneuse;
import data.entites.tours.Mitraillette;
import data.entites.tours.TourType;
import data.environnement.Grille;
import data.manches.GestionnaireManche;
import design.Interface;
import org.lwjgl.input.Mouse;
import org.newdawn.slick.opengl.Texture;

import static assistants.GestionnaireGraphique.*;

// Classe : Jeu
// Regroupe le contenu de la partie

public class Jeu
{
    // Composants
    private Grille grille;
    private Joueur joueur;
    private GestionnaireManche gestionnaireManche;
    private Interface jeuInterface;
    private Interface.Menu toursMenu;
    private Texture menuFond;
    private Ennemi[] ennemisType;

    public Jeu(Grille carte)
    {
        this.grille = carte;

        ennemisType = new Ennemi[2];
        ennemisType[0] = new Drone(2, 0, grille);
        ennemisType[1] = new Soucoupe(2, 0, grille);
        gestionnaireManche = new GestionnaireManche(ennemisType, 3, 3);
        joueur = new Joueur(grille, gestionnaireManche);
        joueur.Configuration();

        menuFond = ChargerRapide("toursFond");
        ConfigurerInterface();
    }

    public void Maj()
    {
        Dessiner();
        gestionnaireManche.Maj();
        joueur.Maj();
        MajInterface();
    }

    public void Dessiner()
    {
        DessinerTexture(menuFond, 1280, 0, 192, 960);
        grille.Dessiner();
    }

    public void ConfigurerInterface()
    {
        jeuInterface = new Interface();

        jeuInterface.CreerMenu("toursMenu", 1280, 50, 192,  960,
                2, 0);

        toursMenu = jeuInterface.getMenu("toursMenu");

        toursMenu.AjouterRapide("Canon", "TourOrangeEntiere");
        toursMenu.AjouterRapide("Mitraillette", "TourRougeEntiere");
        toursMenu.AjouterRapide("Hiverneuse", "HiverneuseEntiere");
    }

    private void MajInterface()
    {
        jeuInterface.Dessiner();

        jeuInterface.DessinerTexte(1300, 840, "Coeurs  : " + Joueur.vies);
        jeuInterface.DessinerTexte(1300, 880, "Pièces    : " + Joueur.argent);
        jeuInterface.DessinerTexte(1300, 920, "Manche : " + gestionnaireManche.getNombreManche());
        jeuInterface.DessinerTexte(10, 0, "FPS : " + GestionnaireEtat.imagesSecondePrecedente);

        if (Mouse.next())
        {
            boolean clicGauche = Mouse.isButtonDown(0);

            if (toursMenu.BoutonClic("Canon"))
            {
                if (clicGauche)
                    joueur.PrendreTour(new Hiverneuse(TourType.TourOrange, grille.getCase(0, 0),
                            gestionnaireManche.getMancheActuelle().getListeEnnemis()));
            }

            if (toursMenu.BoutonClic("Mitraillette"))
            {
                if (clicGauche)
                    joueur.PrendreTour(new Mitraillette(TourType.TourRouge, grille.getCase(0, 0),
                            gestionnaireManche.getMancheActuelle().getListeEnnemis()));
            }

            if (toursMenu.BoutonClic("Hiverneuse"))
            {
                if (clicGauche)
                    joueur.PrendreTour(new Hiverneuse(TourType.Hiverneuse, grille.getCase(0, 0),
                            gestionnaireManche.getMancheActuelle().getListeEnnemis()));
            }

            clicGauche = false;
        }
    }
}
