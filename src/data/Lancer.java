package data;

import assistants.GestionnaireEtat;
import assistants.Horloge;
import org.lwjgl.opengl.Display;

import java.io.File;

import static assistants.GestionnaireGraphique.*;

// Classe Lancer
// Lance le programme

public class Lancer
{
    public static void main(String[] args)
    {
        System.setProperty("java.library.path", "libs");
        //Extracted from Distributing Your LWJGL Application
        System.setProperty("org.lwjgl.librarypath", new File("libs/natives").getAbsolutePath());
        new Lancer();
    }

    public Lancer()
    {
        // Initialiser LWJGL
        LancerSession();

        while (!Display.isCloseRequested()) // Tant qu'aucune demande de fermeture de la fenêtre
        {
            Horloge.Maj();
            GestionnaireEtat.Maj();
            Display.update();
            Display.sync(FPS); // Image secondes
        }
        Display.destroy();
    }
}
