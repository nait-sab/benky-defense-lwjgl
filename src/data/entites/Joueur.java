package data.entites;

import assistants.GestionnaireEtat;
import assistants.Horloge;
import data.entites.tours.Hiverneuse;
import data.environnement.Case;
import data.environnement.Grille;
import data.manches.GestionnaireManche;
import data.entites.tours.Canon;
import data.entites.tours.Tour;
import data.entites.tours.TourType;
import org.lwjgl.Sys;
import org.lwjgl.input.Keyboard;
import org.lwjgl.input.Mouse;

import java.util.ArrayList;

import static assistants.GestionnaireGraphique.*;

// Classe : Joueur
// Gérer les actions de l'utilisateurs

public class Joueur
{
    // Variables
    public static int argent, vies;
    private boolean clicGaucheActif, clicDroitActif, tourAttendue;

    // Composants
    private Grille grille;
    private Tour tourTemporaire;
    private GestionnaireManche gestionnaireManche;

    // Liste
    private ArrayList<Tour> tours;

    public Joueur(Grille grille, GestionnaireManche gestionnaireManche)
    {
        this.grille = grille;
        this.gestionnaireManche = gestionnaireManche;

        argent = vies = 0;
        clicGaucheActif = false;
        clicDroitActif = false;

        tourAttendue = false;
        tourTemporaire = null;
        tours = new ArrayList<Tour>();
    }

    public void Maj()
    {
        // Mise à jour de la tour temporaire
        if (tourAttendue)
        {
            Case caseTemporaire = getCaseCible();

            tourTemporaire.setxPosition(caseTemporaire.getxPosition());
            tourTemporaire.setyPosition(caseTemporaire.getyPosition());
            tourTemporaire.Dessiner();
        }

        for (Tour tour : tours)
        {
            tour.Maj();
            tour.Dessiner();
            tour.DefinirListeEnnemis(gestionnaireManche.getMancheActuelle().getListeEnnemis());
        }

        // Souris événement : 0 - clic gauche / 1 - clic droit

        // Pose une tour sur la case ciblé
        if (Mouse.isButtonDown(0) && !clicGaucheActif)
        {
            PlacerTour();
        }
        if (Mouse.isButtonDown(1) && !clicDroitActif)
        {
            System.out.println(argent);
        }

        // Clavier événements
        while (Keyboard.next())
        {
            // Touche -
            // Ralenti le temps du jeu
            if (Keyboard.getEventKey() == Keyboard.KEY_DOWN && Keyboard.getEventKeyState())
                Horloge.ChangerMultiplicateur(-0.5f);

            // Touche +
            // Accélère le temps du jeu
            if (Keyboard.getEventKey() == Keyboard.KEY_UP && Keyboard.getEventKeyState())
                Horloge.ChangerMultiplicateur(0.5f);

            // Touche Echap
            // Revenir au menu principal
            if (Keyboard.getEventKey() == Keyboard.KEY_ESCAPE && Keyboard.getEventKeyState())
                GestionnaireEtat.DefinirEtat(GestionnaireEtat.JeuEtat.MenuPrincipal);
        }

        // Bloquer les répétitions d'événements
        clicGaucheActif = Mouse.isButtonDown(0); // Clic gauche
        clicDroitActif = Mouse.isButtonDown(1); // Clic droit
    }

    public void Configuration()
    {
        argent = 60;
        vies = 10;
    }

    public static boolean ModifierArgent(int montant)
    {
        if (argent + montant >= 0)
        {
            argent += montant;
            return true;
        }
        return false;
    }

    public static void ModifierVies(int montant)
    {
        vies += montant;
    }

    private void PlacerTour()
    {
        Case cible = getCaseCible();

        if (tourAttendue)
        {
            if (!cible.getUtilisable() && ModifierArgent(-tourTemporaire.getType().getPrix()))
            {
                tours.add(tourTemporaire);
                cible.setUtilisable(true);
                tourAttendue = false;
                tourTemporaire = null;
            }
        }
    }

    public void PrendreTour(Tour tour)
    {
        tourTemporaire = tour;
        tourAttendue = true;
    }

    // Accesseurs / Modifieurs
    private Case getCaseCible()
    {
        return grille.getCase(Mouse.getX() / TAILLECASE, (HAUTEUR - Mouse.getY() - 1) / TAILLECASE);
    }
}
