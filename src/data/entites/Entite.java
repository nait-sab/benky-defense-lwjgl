package data.entites;

public interface Entite
{
    public float getxPosition();
    public float getyPosition();
    public int getLongueur();
    public int getHauteur();

    public void setxPosition(float x);
    public void setyPosition(float y);
    public void setLongueur(int longueur);
    public void setHauteur(int hauteur);

    public void Maj();
    public void Dessiner();
}
