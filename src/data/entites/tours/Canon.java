package data.entites.tours;

import data.entites.Ennemis.Ennemi;
import data.entites.projectiles.BouletCanon;
import data.environnement.Case;

import java.util.concurrent.CopyOnWriteArrayList;

public class Canon extends Tour
{
    public Canon(TourType type, Case emplacement, CopyOnWriteArrayList<Ennemi> ennemis)
    {
        super(type, emplacement, ennemis);
    }

    @Override
    public void Tirer(Ennemi cible)
    {
        super.getProjectiles().add(new BouletCanon(super.getType().getProjectile(), super.getCible(),
                super.getxPosition(), super.getyPosition(),
                super.getLongueur() / 2, super.getHauteur() / 2));
    }
}
