package data.entites.tours;

import data.entites.Ennemis.Ennemi;
import data.entites.Entite;
import data.environnement.Case;
import data.entites.projectiles.Projectile;
import org.newdawn.slick.opengl.Texture;

import java.util.ArrayList;
import java.util.concurrent.CopyOnWriteArrayList;

import static assistants.GestionnaireGraphique.*;
import static assistants.Horloge.Temps;

public abstract class Tour implements Entite
{
    // Variables
    private float xPosition, yPosition, vitesseTir, angle, tempsDernierTir;
    private int longueur, hauteur, portee;
    private boolean cibleDetecte;

    // Composants
    private Texture[] textures;
    private Ennemi cible;
    private TourType type;

    // Liste
    private CopyOnWriteArrayList<Ennemi> ennemis;
    private ArrayList<Projectile> projectiles;

    // Constructeur
    public Tour(TourType type, Case emplacement, CopyOnWriteArrayList<Ennemi> ennemis)
    {
        this.type = type;
        textures = type.getTextures();
        portee = type.getPortee();
        vitesseTir = type.getVitesseTir();

        xPosition = emplacement.getxPosition();
        yPosition = emplacement.getyPosition();
        longueur = emplacement.getLongueur();
        hauteur = emplacement.getHauteur();

        this.ennemis = ennemis;

        cibleDetecte = false;
        tempsDernierTir = 0f;
        angle = 0f;
        projectiles = new ArrayList<Projectile>();
    }

    // Mise à jour
    public void Maj()
    {
        if (!cibleDetecte)
            cible = DefinirCible();
        else
        {
            angle = CalculerAngle();
            if (tempsDernierTir > vitesseTir)
            {
                Tirer(cible);
                tempsDernierTir = 0;
            }
        }

        if (cible == null || cible.getVivant() == false)
            cibleDetecte = false;

        tempsDernierTir += Temps();

        for (Projectile projectile : projectiles)
            projectile.Maj();

        Dessiner();
    }

    // Rendu
    public void Dessiner()
    {
        DessinerTexture(textures[0], xPosition, yPosition, longueur, hauteur);

        if (textures.length > 1)
        {
            for (int nombre = 1; nombre < textures.length; nombre++)
                DessinerTextureRotatif(textures[nombre], xPosition, yPosition, longueur, hauteur, angle);
        }
    }

    // Fonctions
    // Tirer
    // Créer un projectile vers la cible en relançant le chrono du tir
    public abstract void Tirer(Ennemi cible);

    // Définir la cible
    //Cherche une cible la plus proche dans sa portée autorisé
    private Ennemi DefinirCible()
    {
        Ennemi cible = null;
        float cibleDistance = 10000;
        for (Ennemi ennemi : ennemis)
        {
            if (CiblePortee(ennemi) && CalculerDistance(ennemi) < cibleDistance && ennemi.getVivant())
            {
                cibleDistance = CalculerDistance(ennemi);
                cible = ennemi;
            }
        }
        if (cible != null)
            cibleDetecte = true;
        return cible;
    }

    // Portée de la cible
    // Calculer si la cible est à portée
    private boolean CiblePortee(Ennemi cible)
    {
        float xDistance = Math.abs(cible.getxPosition() - xPosition);
        float yDistance = Math.abs(cible.getyPosition() - yPosition);
        if (xDistance < portee && yDistance < portee)
            return true;
        return false;
    }

    // Calculer la distance
    // Déterminer la distance de la cible
    private float CalculerDistance(Ennemi cible)
    {
        float xDistance = Math.abs(cible.getxPosition() - xPosition);
        float yDistance = Math.abs(cible.getyPosition() - yPosition);
        return xDistance + yDistance;
    }

    // Calculer l'angle
    // Déterminer l'angle de la cible pour la rotation du canon
    private float CalculerAngle()
    {
        double angleTemporaire = Math.atan2(cible.getyPosition() - yPosition, cible.getxPosition() - xPosition);
        return (float) Math.toDegrees(angleTemporaire) - 90;
    }

    // Accesseurs
    public TourType getType() { return type; }
    public float getxPosition() { return xPosition; }
    public float getyPosition() { return yPosition; }
    public float getAngle() { return angle; }
    public int getLongueur() { return longueur; }
    public int getHauteur() { return hauteur; }
    public Ennemi getCible() { return cible; }
    public ArrayList<Projectile> getProjectiles() { return projectiles; }

    // Modifieurs
    public void setxPosition(float xPosition) { this.xPosition = xPosition; }
    public void setyPosition(float yPosition) { this.yPosition = yPosition; }
    public void setLongueur(int longueur) { this.longueur = longueur; }
    public void setHauteur(int hauteur) { this.hauteur = hauteur; }
    public void DefinirListeEnnemis(CopyOnWriteArrayList<Ennemi> ennemis) { this.ennemis = ennemis; }
}
