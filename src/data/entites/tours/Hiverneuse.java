package data.entites.tours;

import data.entites.Ennemis.Ennemi;
import data.entites.projectiles.Flocon;
import data.environnement.Case;

import java.util.concurrent.CopyOnWriteArrayList;

public class Hiverneuse extends Tour
{
    public Hiverneuse(TourType type, Case emplacement, CopyOnWriteArrayList<Ennemi> ennemis)
    {
        super(type, emplacement, ennemis);
    }

    @Override
    public void Tirer(Ennemi cible)
    {
        super.getProjectiles().add(new Flocon(super.getType().getProjectile(), super.getCible(),
                super.getxPosition(), super.getyPosition(),
                super.getLongueur() / 2, super.getHauteur() / 2));
    }
}
