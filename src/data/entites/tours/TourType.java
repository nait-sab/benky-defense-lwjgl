package data.entites.tours;

import data.entites.projectiles.ProjectileType;
import org.newdawn.slick.opengl.Texture;
import static assistants.GestionnaireGraphique.*;

public enum TourType
{
    TourOrange(new Texture[]{ChargerRapide("TourOrange"), ChargerRapide("CanonOrange")},
            ProjectileType.BouletCanon, 10, 1000, 10, 3),
    TourRouge(new Texture[]{ChargerRapide("TourRouge"), ChargerRapide("CanonRouge")},
            ProjectileType.Balle, 1, 1000, 15, 0.25f),
    Hiverneuse(new Texture[]{ChargerRapide("HiverneuseBase"), ChargerRapide("HiverneuseCanon")},
            ProjectileType.Flocon, 30, 1000, 20, 2);

    private Texture[] textures;
    private ProjectileType projectile;
    private int degats, portee, prix;
    float vitesseTir;

    TourType(Texture[] textures, ProjectileType projectile, int degats, int portee, int prix, float vitesseTir)
    {
        this.textures = textures;
        this.projectile = projectile;
        this.degats = degats;
        this.portee = portee;
        this.prix = prix;
        this.vitesseTir = vitesseTir;
    }

    // Accesseurs
    public Texture[] getTextures() { return textures; }
    public ProjectileType getProjectile() { return projectile; }
    public int getDegats() { return degats; }
    public int getPrix() { return prix; }
    public int getPortee() { return portee; }
    public float getVitesseTir() { return vitesseTir; }
}
