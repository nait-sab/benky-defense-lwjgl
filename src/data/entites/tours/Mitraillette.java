package data.entites.tours;

import data.entites.Ennemis.Ennemi;
import data.entites.projectiles.Balle;
import data.environnement.Case;

import java.util.concurrent.CopyOnWriteArrayList;

public class Mitraillette extends Tour
{
    public Mitraillette(TourType type, Case emplacement, CopyOnWriteArrayList<Ennemi> ennemis)
    {
        super(type, emplacement, ennemis);
    }

    @Override
    public void Tirer(Ennemi cible)
    {
        super.getProjectiles().add(new Balle(super.getType().getProjectile(), super.getCible(),
                super.getxPosition(), super.getyPosition(),
                super.getLongueur() / 2, super.getHauteur() / 2, getAngle()));
    }
}
