package data.entites.Ennemis;

import static assistants.GestionnaireGraphique.*;
import static assistants.Horloge.*;

import data.entites.Entite;
import data.entites.Joueur;
import data.environnement.Case;
import data.environnement.Grille;
import data.environnement.PointControle;
import org.newdawn.slick.opengl.Texture;

import java.util.ArrayList;

// Classe : Ennemi
// Contient les informations et fonctions des ennemis

public class Ennemi implements Entite
{
    // Variables
    private int longueur, hauteur, controleActuel;
    private float vitesse, xPosition, yPosition, sante, santeRestante, santeInvisible;
    private boolean premier, vivant;
    private int[] directions;

    // Composants
    private Texture texture, santeFond, santeCouleur, santeBords;
    private Case caseCreation;
    private Grille grille;

    private ArrayList<PointControle> listeControles;

    public Ennemi(int xCase, int yCase, Grille grille)
    {
        texture = ChargerRapide("drone");
        this.grille = grille;
        santeFond = ChargerRapide("santeFond");
        santeCouleur = ChargerRapide("santeCouleur");
        santeBords = ChargerRapide("santeBords");
        caseCreation = grille.getCase(xCase, yCase);
        xPosition = caseCreation.getxPosition();
        yPosition = caseCreation.getyPosition();
        longueur = TAILLECASE;
        hauteur = TAILLECASE;
        vitesse = 50;
        sante = santeRestante = santeInvisible = 50;

        premier = true;
        vivant = true;

        listeControles = new ArrayList<PointControle>();
        directions = new int[2];
        directions[0] = 0; // Direction X
        directions[1] = 0; // Direction Y
        directions = ChercherDirection(this.caseCreation);
        controleActuel = 0;
        GenererListeControles();
    }

    public Ennemi(Texture texture, Case caseCreation,Grille grille, int longueur, int hauteur, float vitesse, float sante)
    {
        this.texture = texture;
        santeFond = ChargerRapide("santeFond");
        santeCouleur = ChargerRapide("santeCouleur");
        santeBords = ChargerRapide("santeBords");
        this.caseCreation = caseCreation;
        this.grille= grille;
        xPosition = caseCreation.getxPosition();
        yPosition = caseCreation.getyPosition();
        this.longueur = longueur;
        this.hauteur = hauteur;
        this.vitesse = vitesse;
        this.sante = santeRestante = santeInvisible = sante;

        premier = true;
        vivant = true;

        listeControles = new ArrayList<PointControle>();
        directions = new int[2];
        directions[0] = 0; // Direction X
        directions[1] = 0; // Direction Y
        directions = ChercherDirection(this.caseCreation);
        controleActuel = 0;
        GenererListeControles();
    }

    // Fonction : Mise à jour
    public void Maj()
    {
        if (premier) premier = !premier;
        else
        {
            if (ControleAtteint())
            {
                if (controleActuel + 1 != listeControles.size()) controleActuel++;
                else LimiteAtteinte();
            }
            else
            {
                xPosition += Temps() * listeControles.get(controleActuel).getxDirection() * vitesse;
                yPosition += Temps() * listeControles.get(controleActuel).getyDirection() * vitesse;
            }
        }
    }

    // Fonction : Dessiner
    // Utiliser la fonction de l'artiste pour déssiner l'ennemi
    public void Dessiner()
    {
        float santePourcentage = santeRestante / sante;

        DessinerTexture(texture, xPosition, yPosition, longueur, hauteur);
        DessinerTexture(santeFond, xPosition, yPosition - TAILLECASE / 4, longueur,  TAILLECASE / 8);
        DessinerTexture(santeCouleur, xPosition, yPosition - TAILLECASE / 4,
                TAILLECASE * santePourcentage,  TAILLECASE / 8);
        DessinerTexture(santeBords, xPosition, yPosition - TAILLECASE / 4, longueur,  TAILLECASE / 8);
    }

    private void Mourir()
    {
        vivant = false;
    }

    private void LimiteAtteinte()
    {
        Joueur.ModifierVies(-1);
        Mourir();
    }

    public void Degats(int degats)
    {
        santeRestante -= degats;
        if (santeRestante <= 0)
        {
            Mourir();
            Joueur.ModifierArgent(5);
        }
    }

    private int[] ChercherDirection(Case emplacement)
    {
        int[] direction = new int[2];

        // Cases relié à l'emplacement
        Case haut = grille.getCase(emplacement.getxEmplacement(), emplacement.getyEmplacement() - 1);
        Case droite = grille.getCase(emplacement.getxEmplacement() + 1, emplacement.getyEmplacement());
        Case bas = grille.getCase(emplacement.getxEmplacement(), emplacement.getyEmplacement() + 1);
        Case gauche = grille.getCase(emplacement.getxEmplacement() - 1, emplacement.getyEmplacement());

        if (emplacement.getType() == haut.getType() && directions[1] != 1)
        {
            direction[0] = 0;
            direction[1] = -1;
        }
        else if (emplacement.getType() == droite.getType() && directions[0] != -1)
        {
            direction[0] = 1;
            direction[1] = 0;
        }
        else if (emplacement.getType() == bas.getType() && directions[1] != -1)
        {
            direction[0] = 0;
            direction[1] = 1;
        }
        else if (emplacement.getType() == gauche.getType() && directions[0] != 1)
        {
            direction[0] = -1;
            direction[1] = 0;
        }
        else
        {
            direction[0] = 2;
            direction[1] = 2;
        }

        return direction;
    }

    private PointControle ChercherControle(Case emplacement, int[] direction)
    {
        Case destination = null;
        PointControle controle = null;

        boolean trouver = false;
        int compteur = 1; // Nombre de case

        while (!trouver)
        {
            if (emplacement.getxEmplacement() + direction[0] * compteur == grille.getLongueurBord() ||
                    emplacement.getyEmplacement() + direction[1] * compteur == grille.getHauteurBord() ||
                    emplacement.getType() != grille.getCase(emplacement.getxEmplacement() + direction[0] * compteur,
                    emplacement.getyEmplacement() + direction[1] * compteur).getType())
            {
                trouver = true;
                compteur -= 1;
                destination = grille.getCase(emplacement.getxEmplacement() + direction[0] * compteur,
                        emplacement.getyEmplacement() + direction[1] * compteur);
            }

            compteur++;
        }

        controle = new PointControle(destination, direction[0], direction[1]);
        return controle;
    }

    private void GenererListeControles()
    {
        listeControles.add(ChercherControle(caseCreation, directions = ChercherDirection(caseCreation)));

        int compteur = 0;
        boolean continuer = true;

        while (continuer)
        {
            int[] directionActuelle = ChercherDirection(listeControles.get(compteur).getCase());
            if (directionActuelle[0] == 2 || compteur == 20)
            {
                continuer = false;
            }
            else
            {
                listeControles.add(ChercherControle(listeControles.get(compteur).getCase(),
                        directions = ChercherDirection(listeControles.get(compteur).getCase())));
            }
            compteur++;
        }
    }

    private boolean ControleAtteint()
    {
        boolean atteint = false;

        Case cible = listeControles.get(controleActuel).getCase();

        if (xPosition > cible.getxPosition() - 3 && xPosition < cible.getxPosition() + 3 &&
        yPosition > cible.getyPosition() - 3 && yPosition < cible.getyPosition() + 3)
        {
            atteint = true;
            xPosition = cible.getxPosition();
            yPosition = cible.getyPosition();
        }

        return atteint;
    }

    public void ReduireSanteInvisible(float total) { santeInvisible -= total; }

    // Accesseur
    public Texture getTexture() { return texture; }
    public Case getCaseCreation() { return caseCreation; }
    public Grille getGrille() { return grille; }
    public float getxPosition() { return xPosition; }
    public float getyPosition() { return yPosition; }
    public int getLongueur() { return longueur; }
    public int getHauteur() { return hauteur; }
    public float getSante() { return sante; }
    public float getSanteInvisible() { return santeInvisible; }
    public float getVitesse() { return vitesse; }
    public boolean getVivant() { return vivant; }

    // Modifieurs
    public void setTexture(String nom) { texture = ChargerRapide(nom); }
    public void setCaseCreation(Case emplacement) { caseCreation = emplacement; }
    public void setGrille(Grille grille) { this.grille = grille; }
    public void setxPosition(float xPosition) { this.xPosition = xPosition; }
    public void setyPosition(float yPosition) { this.yPosition = yPosition; }
    public void setLongueur(int longueur) { this.longueur = longueur; }
    public void setHauteur(int hauteur) { this.hauteur = hauteur; }
    public void setSante(float sante) { this.sante = sante; }
    public void setVitesse(int vitesse) { this.vitesse = vitesse; }

}
