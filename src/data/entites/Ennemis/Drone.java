package data.entites.Ennemis;

import data.environnement.Grille;

public class Drone extends Ennemi
{
    public Drone(int xCase, int yCase, Grille grille)
    {
        super(xCase, yCase, grille);
    }
}
