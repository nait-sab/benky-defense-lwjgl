package data.entites.Ennemis;

import data.environnement.Grille;

import static assistants.GestionnaireGraphique.*;

public class Soucoupe extends Ennemi
{
    public Soucoupe(int xCase, int yCase, Grille grille)
    {
        super(xCase, yCase, grille);
        setTexture("Soucoupe");
        setVitesse(80);
    }
}
