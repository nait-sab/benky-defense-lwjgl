package data.entites.projectiles;

import org.newdawn.slick.opengl.Texture;
import static assistants.GestionnaireGraphique.*;

public enum ProjectileType
{
    BouletCanon(ChargerRapide("BouletCanon"), 10, 600),
    Balle(ChargerRapide("Balle"), 1, 1200),
    Flocon(ChargerRapide("Flocon"), 6, 450);

    private Texture texture;
    private int degats;
    float vitesse;

    ProjectileType(Texture texture, int degats, float vitesse)
    {
        this.texture = texture;
        this.degats = degats;
        this.vitesse = vitesse;
    }

    // Accesseurs
    public Texture getTexture() { return texture; }
    public int getDegats() { return degats; }
    public float getVitesse() { return vitesse; }
}
