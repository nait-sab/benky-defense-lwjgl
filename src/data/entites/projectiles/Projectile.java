package data.entites.projectiles;

import data.entites.Ennemis.Ennemi;
import data.entites.Entite;
import org.newdawn.slick.opengl.Texture;

import static assistants.Horloge.*;
import static assistants.GestionnaireGraphique.*;

// Classe : Projectile
// Contient les informations des balles tiré par les tours

public abstract class Projectile implements Entite
{
    // Variables
    private float xPosition, yPosition, vitesse, xVitesse, yVitesse, angle;
    private int longueur, hauteur, degats;
    private boolean vivant;

    // Composants
    private Texture texture;
    private Ennemi cible;

    // Constructeur
    public Projectile(ProjectileType type, Ennemi cible, float xPosition, float yPosition, int longueur, int hauteur)
    {
        this.texture = type.getTexture();
        this.degats = type.getDegats();
        this.vitesse = type.getVitesse();
        this.cible = cible;
        this.xPosition = xPosition;
        this.yPosition = yPosition;
        this.longueur = longueur;
        this.hauteur= hauteur;

        vivant = true;
        xVitesse = 0f;
        yVitesse = 0f;
        angle = 0f;
        CalculerDirection();
    }

    // Fonction : Mise à jour
    public void Maj()
    {
        if (vivant)
        {
            xPosition += Temps() * vitesse * xVitesse;
            yPosition += Temps() * vitesse * yVitesse;

            if (VerifierCollision(xPosition, yPosition, longueur, hauteur,
                    cible.getxPosition(), cible.getyPosition(), cible.getLongueur(), cible.getHauteur()))
            {
                Degats();
            }

            Dessiner();
        }
    }

    public void Degats()
    {
        vivant = false;
        cible.Degats(degats);
    }

    // Fonction : Dessiner
    public void Dessiner()
    {
        DessinerTexture(texture, xPosition, yPosition, TAILLECASE / 2, TAILLECASE / 2);
    }

    // Fonction : Calculer la direction
    // Sert à déterminer la direction dans laquelle se diriger (cible actuelle)
    private void CalculerDirection()
    {
        float mouvementAutoriser = 1.0f;

        float xDistanceCible = Math.abs(cible.getxPosition() - xPosition + TAILLECASE / 3);
        float yDistanceCible = Math.abs(cible.getyPosition() - yPosition + TAILLECASE / 3);
        float distanceTotaleCible = xDistanceCible + yDistanceCible;

        float xPourcentMouvement = xDistanceCible / distanceTotaleCible;
        xVitesse = xPourcentMouvement;
        yVitesse = mouvementAutoriser - xPourcentMouvement;

        if (cible.getxPosition() < xPosition) xVitesse *= -1;
        if (cible.getyPosition() < yPosition) yVitesse *= -1;
    }

    // Accesseurs
    public float getxPosition() { return xPosition; }
    public float getyPosition() { return yPosition; }
    public int getLongueur() { return longueur; }
    public int getHauteur() { return hauteur; }
    public Texture getTexture() { return texture; }
    public Ennemi getCible() { return cible; }

    // Modifieurs
    public void setxPosition(float xPosition) { this.xPosition = xPosition; }
    public void setyPosition(float yPosition) { this.yPosition = yPosition; }
    public void setLongueur(int longueur) { this.longueur = longueur; }
    public void setHauteur(int hauteur) { this.hauteur = hauteur; }
}
