package data.entites.projectiles;

import data.entites.Ennemis.Ennemi;

import static assistants.GestionnaireGraphique.*;

public class Balle extends Projectile
{
    private float angle;

    public Balle(ProjectileType type, Ennemi cible, float xPosition, float yPosition, int longueur, int hauteur, float angle)
    {
        super(type, cible, xPosition, yPosition, longueur, hauteur);
        this.angle = angle;
    }

    @Override
    public void Degats()
    {
        super.Degats();
    }

    @Override
    public void Dessiner() {
        DessinerTextureRotatif(super.getTexture(), super.getxPosition(), super.getyPosition(),
                TAILLECASE / 3, TAILLECASE / 3, angle - 180);
    }
}
