package data.entites.projectiles;

import data.entites.Ennemis.Ennemi;

public class Flocon extends Projectile
{
    public Flocon(ProjectileType type, Ennemi cible, float xPosition, float yPosition, int longueur, int hauteur)
    {
        super(type, cible, xPosition, yPosition, longueur, hauteur);
    }

    @Override
    public void Degats()
    {
        getCible().setVitesse(20);
        super.Degats();
    }
}
