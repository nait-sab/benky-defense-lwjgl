package assistants;

import org.lwjgl.Sys;

// Classe horloge
// Définir et contrôler le temps dans le programme

public class Horloge
{
    private static boolean pause = false;
    public static long derniereImage, tempsTotal;
    public static float temps = 0, multiplicateur = 1;

    // Fonction : Calculer le temps
    // Calculer le temps écoulé entre la dernière image et maintenant
    public static float CalculerTemps()
    {
        long tempsActuel = DemanderHeure();
        int tempsCalculer = (int) (tempsActuel - derniereImage);
        derniereImage = DemanderHeure();

        if (tempsCalculer * 0.001f > 0.05f)
            return 0.05f;

        return tempsCalculer * 0.001f;
    }

    public static void Maj()
    {
        temps = CalculerTemps();
        tempsTotal += temps;
    }

    public static void ChangerMultiplicateur(float vitesse)
    {
        if (!(multiplicateur + vitesse < -1 && multiplicateur + vitesse > 3))
        {
            multiplicateur += vitesse;
        }
    }

    public static void Pause() { pause = !pause; }

    // Accesseurs
    public static long DemanderHeure() { return Sys.getTime() * 1000 / Sys.getTimerResolution(); }
    public static float TempsTotal() { return tempsTotal; }
    public static float Multiplicateur() { return multiplicateur; }
    public static float Temps()
    {
        if (pause) return 0;
        else return temps * multiplicateur;
    }


}
