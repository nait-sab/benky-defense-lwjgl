package assistants;

import data.environnement.Case;
import data.environnement.Grille;
import data.environnement.CaseType;

import java.io.*;

public class GestionnaireNiveau
{
    // Sauvegarder la carte créer en éditeur
    public static void SauvegarderCarte(String nomCarte, Grille grille)
    {
        String carteDonnees = "";

        for (int yCase = 0; yCase < grille.getLongueurBord(); yCase++)
        {
            for (int xCase = 0; xCase < grille.getHauteurBord(); xCase++)
            {
                carteDonnees += getCaseID(grille.getCase(yCase, xCase));
            }
        }

        try {
            File fichier = new File(nomCarte);
            BufferedWriter ecrivain = new BufferedWriter(new FileWriter(fichier));
            ecrivain.write(carteDonnees);
            ecrivain.close();
        } catch (Exception exception) { exception.printStackTrace(); }
    }

    // Générer un ID pour la sauvegarde selon la case demandé
    public static String getCaseID(Case emplacement)
    {
        String ID = "E";

        switch (emplacement.getType())
        {
            case Herbe:
                ID = "0";
                break;

            case Terre:
                ID = "1";
                break;

            case Eau:
                ID = "2";
                break;

            case NULL:
                ID = "3";
                break;
        }

        return ID;
    }

    // Charger une carte d'éditeur
    public static Grille ChargerCarte(String nomCarte)
    {
        Grille grille = new Grille();

        try {
            BufferedReader lecteur = new BufferedReader(new FileReader(nomCarte));
            String donnees = lecteur.readLine();
            for (int yCase = 0; yCase < grille.getLongueurBord(); yCase++)
            {
                for (int xCase = 0; xCase < grille.getHauteurBord(); xCase++)
                {
                    grille.setCase(yCase, xCase, getTerrain(donnees.substring(
                            yCase * grille.getHauteurBord() + xCase,
                            yCase * grille.getHauteurBord() + xCase + 1)));
                }
            }
            lecteur.close();
        } catch (Exception exception) { exception.printStackTrace(); }

        return grille;
    }

    // Récupérer le terrain selon l'ID trouvé dans le fichier chargé
    public static CaseType getTerrain(String ID)
    {
        CaseType type = CaseType.NULL;

        switch (ID)
        {
            case "0":
                type = CaseType.Herbe;
                break;
            case "1":
                type = CaseType.Terre;
                break;
            case "2":
                type = CaseType.Eau;
                break;
            case "3":
                type = CaseType.NULL;
                break;
        }

        return type;
    }
}
