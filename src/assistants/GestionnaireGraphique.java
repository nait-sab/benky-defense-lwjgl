package assistants;

import org.lwjgl.LWJGLException;
import org.lwjgl.opengl.Display;
import org.lwjgl.opengl.DisplayMode;
import org.newdawn.slick.opengl.Texture;
import org.newdawn.slick.opengl.TextureLoader;
import org.newdawn.slick.util.ResourceLoader;

import java.io.IOException;
import java.io.InputStream;

import static org.lwjgl.opengl.GL11.*;

// Classe : Artiste
// Regroupe les fonctions de dessin et les réglages de la fenêtre

public class GestionnaireGraphique
{
    // Variables de configuration
    public static final int LONGUEUR = 1472, HAUTEUR = 960; // > 1280 / 64 = 20 cases et 960 / 64 = 15 cases
    public static final int FPS = 60;
    public static final int TAILLECASE = 64;
    private static final double version = 1.1;
    private static final String TITRE = "Benky Defense - Version : " + version;

    // Initialisateur de LWJGL
    public static void LancerSession()
    {
        // Génération de la fenêtre
        Display.setTitle(TITRE);
        try {
            Display.setDisplayMode(new DisplayMode(LONGUEUR, HAUTEUR));
            Display.create();
        } catch (LWJGLException e) {
            e.printStackTrace();
        }

        // Génération du module LWJGL dans la fenêtre
        glMatrixMode(GL_PROJECTION);
        glLoadIdentity();
        glOrtho(0, LONGUEUR, HAUTEUR, 0, 1, -1);
        glMatrixMode(GL_MODELVIEW);
        glEnable(GL_TEXTURE_2D);
        glEnable(GL_BLEND); // Transparence dans les images
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA); // La régler sur les taux alpha
    }

    // Fonction : Déssiner un carré
    // fonction de test pour désinner un simple carré blanc dans la fenêtre selon les paramètres
    // Vertex = coordonnée du tracage
    // Gl_QUADS pour formé un ensemble de deux triangle sur les coordonnées (2 triangle pour un carré)
    public static void DessinerCarre(float xPosition, float yPosition, float longueur, float hauteur)
    {
        glBegin(GL_QUADS); // Débuter le tracé GL
        glVertex2f(xPosition, yPosition); // top left
        glVertex2f(xPosition + longueur, yPosition); // top right
        glVertex2f(xPosition + longueur, yPosition + hauteur); // bottom right
        glVertex2f(xPosition, yPosition + hauteur); // bottom left
        glEnd(); // Arrêter le tracé GL
    }

    // Fonction : Déssiner une texture
    // fonction identique de ci-dessus mais pour afficher une texture à la place du blanc entier
    // Vertex = coordonnée du tracage
    // Gl_QUADS pour formé un ensemble de deux triangle sur les coordonnées (2 triangle pour un carré)
    public static void DessinerTexture(Texture texture, float xPosition, float yPosition, float longueur, float hauteur)
    {
        texture.bind(); // récupérer la texture
        glTranslatef(xPosition, yPosition, 0); // Placé le tracet sur les coordonnées demandés
        glBegin(GL_QUADS); // Garder le traçage en triangles
        glTexCoord2f(0, 0); // Tex Coord pour les coordonnées de textures
        glVertex2f(0, 0); // Vertex pour les coordonnées de tracé des lignes
        glTexCoord2f(1, 0);
        glVertex2f(longueur, 0);
        glTexCoord2f(1, 1);
        glVertex2f(longueur, hauteur);
        glTexCoord2f(0, 1);
        glVertex2f(0, hauteur);
        glEnd(); // Arrêter le tracé
        glLoadIdentity(); // Appliquer la texture sur la forme généré (Ne pas retirer)
    }

    // Fonction : Déssiner une texture rotatif
    // fonction identique de ci-dessus mais pour afficher une texture capable de tourner sur elle-même
    public static void DessinerTextureRotatif(Texture texture, float xPosition, float yPosition,
                                              float longueur, float hauteur, float angle)
    {
        texture.bind(); // récupérer la texture
        glTranslatef(xPosition + longueur / 2, yPosition + hauteur / 2, 0);
        glRotatef(angle, 0, 0, 1);
        glTranslatef(- longueur / 2, - hauteur / 2, 0);
        glBegin(GL_QUADS); // Garder le traçage en triangles
        glTexCoord2f(0, 0); // Tex Coord pour les coordonnées de textures
        glVertex2f(0, 0); // Vertex pour les coordonnées de tracé des lignes
        glTexCoord2f(1, 0);
        glVertex2f(longueur, 0);
        glTexCoord2f(1, 1);
        glVertex2f(longueur, hauteur);
        glTexCoord2f(0, 1);
        glVertex2f(0, hauteur);
        glEnd(); // Arrêter le tracé
        glLoadIdentity(); // Appliquer la texture sur la forme généré (Ne pas retirer)
    }

    // Fonction : Charger Texture
    // Récupérer la texture dans l'emplacement demandé avec son type de fichier (PNG, JPG, ect...)
    public static Texture ChargerTexture(String emplacement, String typeFichier)
    {
        Texture texture = null;
        InputStream cible = ResourceLoader.getResourceAsStream(emplacement);
        try {
            texture = TextureLoader.getTexture(typeFichier, cible); // Regarder sur le wiki pour les types acceptés
        } catch (IOException e) {
            e.printStackTrace();
        }
        return texture;
    }

    // Fonction : Charger Rapide
    // Utiliser la fonction complête plus simplement en récupérant tout de suite le bon dossier des ressources
    // ! Attention au type de l'image, la fonction marche qu'avec PNG
    public static Texture ChargerRapide(String nom)
    {
        Texture texture = null;
        texture = ChargerTexture("ressources/" + nom + ".png", "PNG");
        return texture;
    }

    public static boolean VerifierCollision(float x1, float y1, float longueur1, float hauteur1,
                                            float x2, float y2, float longueur2, float hauteur2)
    {
        if (x1 + longueur1 > x2 + longueur2 && x1 < x2 + longueur2 && y1 + hauteur1 > y2 && y1 < y2 + hauteur2)
            return true;
        return false;
    }
}
