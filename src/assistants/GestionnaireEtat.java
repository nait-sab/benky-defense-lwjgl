package assistants;

import data.environnement.Grille;
import data.etats.*;
import org.lwjgl.Sys;

import static assistants.GestionnaireNiveau.ChargerCarte;

public class GestionnaireEtat
{
    public static enum JeuEtat
    {
        MenuPrincipal,
        Jeu,
        Editeur
    }

    public static JeuEtat jeuEtat = JeuEtat.MenuPrincipal;

    public static MenuPrincipal menuPrincipal;
    public static Jeu jeu;
    public static Editeur editeur;

    public static long prochaineSeconde = System.currentTimeMillis() + 1000;
    public static int imagesSecondePrecedente = 0;
    public static int imagesSecondeActuelle = 0;

    // Carte du jeu (20 * 15)
    // 0 - Herbe
    // 1 - Terre
    // 2 - Eau
    public static Grille carte = ChargerCarte("carteEditeur");

    public static void Maj()
    {
        switch (jeuEtat)
        {
            case MenuPrincipal:
                if (menuPrincipal == null)
                    menuPrincipal = new MenuPrincipal();
                menuPrincipal.Maj();
                break;

            case Jeu:
                if (jeu == null)
                    jeu = new Jeu(carte);
                jeu.Maj();
                break;

            case Editeur:
                if (editeur == null)
                    editeur = new Editeur();
                editeur.Maj();
                break;
        }

        // CALCUL IMAGES SECONDE (FPS)
        long secondeActuelle = System.currentTimeMillis();
        if (secondeActuelle > prochaineSeconde)
        {
            prochaineSeconde += 1000;
            imagesSecondePrecedente = imagesSecondeActuelle;
            imagesSecondeActuelle = 0;
        }
        imagesSecondeActuelle++;
    }

    public static void DefinirEtat(JeuEtat nouvelEtat)
    {
        if (jeu != null) jeu = null;
        if (editeur != null) editeur = null;

        jeuEtat = nouvelEtat;
    }
}
